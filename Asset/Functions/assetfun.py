# -*- coding: utf-8 -*-
# @Time    : 2020/5/18
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : assetfun.py


from .. import models, serializers
from Base import models as basemodels


def dict2asset(asset_dic):
    """
    :param asset_dic:{'name':'xxx','key':'xxxx@xxx.com','type':'111111111','description':'xxxx', 'is_out':'False','manage':''}
    """
    asset_item = models.Asset.objects.get_or_create(key=asset_dic.get['key'])[0]
    asset_item.name = asset_dic.get['name']
    asset_item.description = asset_dic.get['description']
    type_item = models.Type.objects.filter(name=asset_dic.get['type']).first()
    if type_item:
        asset_item.type = asset_dic.get['type']
    if asset_dic.get['is_out']:
        asset_item.is_out = asset_dic.get['is_out']
    manage_item = basemodels.Person.objects.filter(mail=asset_dic.get['manage']).first()
    if manage_item:
        asset_item.manage = asset_dic.get['manage']
    asset_item.save()


def get_group_list(user_get):
    list_get = models.Group.objects.none()
    if user_get.is_superuser:
        list_get = models.Group.objects.all()
    return list_get


def get_asset_list(user_get):
    list_get = models.Asset.objects.none()
    if user_get.is_superuser:
        list_get = models.Asset.objects.all()
    return list_get


def check_group_permission(group_id, user_get):
    if user_get.is_superuser:
        item_get = models.Group.objects.filter(id=group_id).first()
        return item_get
    return False


def check_asset_permission(asset_id, user_get):
    if user_get.is_superuser:
        item_get = models.Asset.objects.filter(id=asset_id).first()
        return item_get
    return False


def check_plugin_permission(plugin_id, user_get):
    if user_get.is_superuser:
        item_get = models.PluginInfo.objects.filter(id=plugin_id).first()
        return item_get
    return False


def check_port_permission(port_id, user_get):
    if user_get.is_superuser:
        item_get = models.PortInfo.objects.filter(id=port_id).first()
        return item_get
    return False


def get_asset_details(asset_obj):
    info_key = []
    data = {}
    typeinfo_list = models.TypeInfo.objects.filter(type_connect=asset_obj.type)
    data['info'] = serializers.AssetSerializer(instance=asset_obj).data
    for item in typeinfo_list:
        info_key.append(item.key)
    if 'os' in info_key:
        try:
            os_info = asset_obj.os_for_asset
        except:
            models.OsInfo.objects.get_or_create(asset=asset_obj)
            os_info = asset_obj.os_for_asset
        data['os'] = serializers.OSinfoSerializer(instance=os_info).data
    if 'web' in info_key:
        try:
            web_info = asset_obj.web_for_asset
        except:
            models.WebInfo.objects.get_or_create(asset=asset_obj)
            web_info = asset_obj.web_for_asset
        data['web'] = serializers.WebInfoSerializer(instance=web_info).data
    if 'tag' in info_key:
        tag_list = asset_obj.tag_for_asset.all()
        data['tag'] = serializers.TagInfoSerializer(instance=tag_list, many=True).data
    if 'group' in info_key:
        group_list = asset_obj.asset_2_group.all()
        data['tag'] = serializers.GroupTagSerializer(instance=group_list, many=True).data
    if 'port' in info_key:
        data['port'] = ''
    if 'plugin' in info_key:
        data['plugin'] = ''
    if 'vuln' in info_key:
        data['vuln'] = ''
    return data
