# -*- coding: utf-8 -*-
# @Time    : 2020/7/29
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : publicfun.py

from Task import models
from .base import dnspodfun, xrayfun


def task_scan(task_obj, user):
    task_define = task_obj.predefine
    for item in task_define.police.all():
        models.TaskScan.objects.get_or_create(
            police=item,
            task=task_obj,
            status='待执行',
            user=user
        )
    return True



def start_scan(task_id):
    task_get = models.Task.objects.filter(id=task_id).first()
    if task_get:
        scantask_list = models.TaskScan.objects.filter(task=task_get)
        for item in scantask_list:
            if item.police.scanner.type == 'X-Ray':
                xrayfun.create_scan(item)
                while xrayfun.chekc_status(item):
                    xrayfun.get_scan_res(item)
                    task_get.status = '已完成'
                    task_get.save()
                    return True
    return False


def start_cycle_scan(task_id):
    task_get = models.Task.objects.filter(id=task_id).first()
    if task_get:
        scantask_list = models.TaskScan.objects.filter(task=task_get)
        for item in scantask_list:
            if item.police.scanner.type == 'DnsPod':
                print('get dnspod')
                dnspodfun.get_dnspod_res(item)
        return True
    return False


def pause_scan(task_scan_obj):
    if task_scan_obj.police.scanner.type == 'X-Ray':
        pass
    task_scan_obj.status = '已暂停'
    task_scan_obj.save()
    return True


def continue_scan(task_scan_obj):
    if task_scan_obj.police.scanner.type == 'X-Ray':
        pass
    return True
