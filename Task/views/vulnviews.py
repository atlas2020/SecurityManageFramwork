# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : vulnviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from Vuln import models, serializers
from ..Functions import taskfun
from django.db.models import Q


@api_view(['GET'])
def list_views(request, task_id):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    task_item = taskfun.check_task_permission(task_id, request.user)
    if task_item:
        list_get = models.Vuln.objects.filter(
            Q(name__icontains=key) |
            Q(type__name__icontains=key) |
            Q(level__name__icontains=key),
            task=task_item
        ).order_by('-update_time')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.VulnSerializer(instance=list_page, many=True)
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)
