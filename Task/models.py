# coding:utf-8
from django.db import models
from django.contrib.auth.models import User
from Asset import models as assetmodels
from Base.models import Person
from Scanner import models as scannermodels


# Create your models here.
class Type(models.Model):
    name = models.CharField('分类名称', max_length=50)
    key = models.CharField('分类键值', max_length=50)
    description = models.TextField('分类描述', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = '分类管理'


class PreDefine(models.Model):
    name = models.CharField('预定义扫描名称', max_length=50)
    tasktype = models.ForeignKey(Type, related_name='tasktype_for_predefine', verbose_name='类型关联',
                                 on_delete=models.CASCADE)
    key = models.CharField('预定义扫描键值', max_length=60)
    police = models.ManyToManyField(scannermodels.Policies, related_name='police_for_predefine', verbose_name='扫描策略')
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'PreDefine'
        verbose_name_plural = '预定义扫描'


class Task(models.Model):
    name = models.CharField('任务名称', max_length=30)
    target = models.TextField('任务范围', null=True, blank=True)
    key = models.CharField('异步任务id', max_length=50, null=True, blank=True)
    description = models.TextField('任务说明', null=True, blank=True)
    type = models.ForeignKey(Type, verbose_name='任务类型', related_name='type_for_task', on_delete=models.CASCADE)
    predefine = models.ForeignKey(PreDefine, verbose_name='任务策略', related_name='predefine_for_task', blank=True, null=True, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, verbose_name='业务接口人', related_name='person_for_task', null=True,
                               on_delete=models.SET_NULL)
    manage = models.ManyToManyField(Person, verbose_name='参与人', related_name='manage_for_task')

    asset = models.ManyToManyField(assetmodels.Asset, related_name='asset_for_task')
    user = models.ForeignKey(User, related_name='user_for_Task', verbose_name='创建用户', null=True, blank=True,
                             on_delete=models.SET_NULL)

    cycle_hours = models.IntegerField('等待时间', default=1)
    start_time = models.DateTimeField('计划时间', null=True, blank=True)
    end_time = models.DateTimeField('截止时间', null=True, blank=True)

    add_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Task'
        verbose_name_plural = '任务管理'


class TaskScan(models.Model):
    police = models.ForeignKey(scannermodels.Policies, related_name='police_for_taskscan', verbose_name='扫描策略',
                               on_delete=models.CASCADE)
    task = models.ForeignKey(Task, related_name='task_for_tasksacn', verbose_name='任务关联', on_delete=models.CASCADE)
    scan_id = models.CharField('扫描ID', max_length=50, null=True)
    status = models.CharField('扫描状态', max_length=20)
    user = models.ForeignKey(User, related_name='user_for_taskscan', verbose_name='创建用户', on_delete=models.CASCADE)
    addtime = models.DateTimeField('扫描时间', auto_now_add=True)

    def __str__(self):
        return str(self.addtime)

    class Meta:
        verbose_name = 'TaskScan'
        verbose_name_plural = '扫描任务'


class ApiCode(models.Model):
    code = models.CharField('校验码', max_length=100)
    task = models.ForeignKey(Task, related_name='task_for_apicode', on_delete=models.CASCADE)
    deny_time = models.DateTimeField('失效时间')
    starttime = models.DateTimeField('添加时间', auto_now_add=True)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'ApiCode'
        verbose_name_plural = '机器校验'
