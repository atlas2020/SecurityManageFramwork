# -*- coding: utf-8 -*-
# @Time    : 2020/11/19
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : syncfun.py
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .. import models
from ..Functions import synckeyfun
import json

"""
create_views参数：
{
'type':'vuln|asset',
'info': [{},{}]
}
"""


@api_view(['POST'])
@permission_classes((AllowAny,))
def create_views(request, synckey):
    data = {
        "code": 1,
        "msg": "",
    }
    data_get = json.loads(request.body)
    type_get = data_get.get('type')
    type_obj = models.Type.objects.filter(key=type_get).first()
    key_obj = models.SyncKey.objects.filter(key=synckey, type=type_obj).first()
    if type_obj:
        res = synckeyfun.key2action(data_get.get('info'), key_obj)
        if res:
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '同步失败'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)
