# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('list/echolist/<str:key>/', views.main_list, name='echolist'),
    path('key/<str:key>/', views.key_log, name='key_log'),
    path('echo_clean/<str:key>/', views.echo_clean, name='echo_clean'),
]