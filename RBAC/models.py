# coding:utf-8
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import django.utils.timezone as timezone
from Base import  models as basemodels
from Base.models import Person

# Create your models here.


class Menu(models.Model):
    order = models.IntegerField('排序', default=0)
    name = models.CharField(verbose_name='菜单名称', max_length=100)
    key = models.CharField(verbose_name='菜单标识', max_length=50)
    is_show = models.BooleanField('是否显示', default=True)
    icon = models.CharField(verbose_name='菜单图标', max_length=100, null=True, blank=True)
    jump = models.CharField(verbose_name='路由地址', max_length=200, null=True, blank=True)
    component = models.CharField(verbose_name='跳转地址', max_length=200, null=True, blank=True)
    parent = models.ForeignKey('self', verbose_name='上级菜单', related_name='menu_menu', null=True, blank=True,
                               on_delete=models.SET_NULL)

    def __str__(self):
        title_list = [self.name]
        p = self.parent
        while p:
            title_list.insert(0, p.name)
            p = p.parent
        return '-'.join(title_list)

    class Meta:
        verbose_name = 'Menu'
        verbose_name_plural = '菜单管理'


class Permission(models.Model):
    name = models.CharField(verbose_name='权限名称', max_length=100)
    url = models.CharField(verbose_name='授权地址', max_length=200)
    menu = models.ForeignKey(Menu, verbose_name='菜单关联', related_name='permission_menu', null=True,
                             on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Permission'
        verbose_name_plural = '权限关联'


class Role(models.Model):
    name = models.CharField('角色名称', max_length=25, unique=True)
    description = models.TextField('角色描述', null=True, blank=True)
    menu = models.ManyToManyField(Menu, verbose_name='角色关联', related_name='role_menu')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Role'
        verbose_name_plural = '角色管理'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    title = models.CharField('名称', max_length=50, null=True, blank=True)
    description = models.TextField('单位描述', null=True, blank=True)

    is_resetpsd = models.BooleanField('强制修改密码', default=True)

    error_count = models.IntegerField('错误登录', default=0)
    login_count = models.IntegerField('登录统计', default=0)
    lock_time = models.DateTimeField('锁定时间', default=timezone.now)

    person = models.ForeignKey(Person, related_name='person_for_user', verbose_name='用户关联', null=True, blank=True,
                             on_delete=models.SET_NULL)
    roles = models.ForeignKey(Role, verbose_name='用户角色', related_name='role_for_user', null=True, on_delete=models.CASCADE)
    permissionmanage = models.ManyToManyField(basemodels.PermissionManage, verbose_name='管理范围', related_name='permissionmanage_for_user')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = '用户属性'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.get_or_create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()