# coding:utf-8
"""
Created on 2018年11月8日

@author: 残源
"""
from django.utils.html import escape


def menutotree(menu_list, is_root=True):
    menu_tree = []
    for menu_get in menu_list:
        menu_node = {
            'name': escape(menu_get.key)
            , 'title': escape(menu_get.name)
            , 'icon': escape(menu_get.icon)
            , 'list': []
        }
        if menu_get.menu_menu.all():
            menu_node['list'] = menutotree(menu_get.menu_menu.all().order_by('id'), False)
        if not is_root:
            menu_node['jump'] = escape(menu_get.jump)
        menu_tree.append(menu_node)
    return menu_tree


def menutotree_easyweb(menu_list, is_root=True):
    menu_tree = []
    for menu_get in menu_list:
        if is_root:
            menu_node = {
                'name': escape(menu_get.name)
                , 'url': escape(menu_get.jump)
                , 'show': menu_get.is_show
                , 'icon': escape(menu_get.icon)
                , 'subMenus': []
            }
        else:
            menu_node = {
                'name': escape(menu_get.name)
                , 'url': escape(menu_get.jump)
                , 'show': menu_get.is_show
                , 'subMenus': []
            }
        if menu_get.menu_menu.all():
            menu_node['subMenus'] = menutotree_easyweb(menu_get.menu_menu.all(), False)
        if not is_root:
            menu_node['url'] = escape(menu_get.jump)
        menu_tree.append(menu_node)
    return menu_tree


def menutotree_vue(menu_list, is_root=True):
    menu_tree = []
    for menu_get in menu_list:
        menu_node = {
            'title': escape(menu_get.name)
            , 'is_show': escape(menu_get.is_show)
            , 'icon': escape(menu_get.icon)
            , 'path': escape(menu_get.jump)
            , 'component': escape(menu_get.component)
        }
        if menu_get.menu_menu.all():
            menu_node['children'] = menutotree_vue(menu_get.menu_menu.all().order_by('id'), False)
        if not is_root:
            menu_node['path'] = escape(menu_get.jump)
            menu_node['component'] = escape(menu_get.component)
        menu_tree.append(menu_node)
    return menu_tree
