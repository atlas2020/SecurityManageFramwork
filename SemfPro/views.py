# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : views.py

from django.http import HttpResponseRedirect
# from django.http import JsonResponse
from captcha.conf import settings
from captcha.helpers import captcha_image_url, captcha_audio_url
from captcha.models import CaptchaStore
from django.http import HttpResponse, Http404
import json

# Create your views here.


def index(request):
    return HttpResponseRedirect('/static/index.html')
