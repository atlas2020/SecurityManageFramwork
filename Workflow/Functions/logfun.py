# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : logfun.py
from .. import models


def create_workflowlog(data):
    """
    data={
        'user':'user_obj',
        'ticket':'ticket_obj',
        'description':'description'
        }
    """
    models.WorkflowLog.objects.create(
        description=data.get('description'),
        user=data.get('user'),
        ticket=data.get('ticket')
    )
    return True