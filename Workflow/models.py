#coding:utf-8
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Workflow(models.Model):
    key = models.CharField('流程key', max_length=30)
    name = models.CharField('审批流程', max_length=30)
    description = models.CharField('状态说明', max_length=30)
    is_check = models.BooleanField('流程流转', default=True)
    status = models.CharField('流程状态', max_length=30)
    parent = models.ForeignKey('self', verbose_name='上级节点', related_name='workflow_to_workflow', null=True, blank=True,
                               on_delete=models.CASCADE)

    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        # 显示层级菜单
        return self.name

    class Meta:
        verbose_name = 'Workflow'
        verbose_name_plural = '审批流程'


class Tickets(models.Model):
    name = models.CharField('单据名称', max_length=1000)
    ticket_key = models.CharField('单据键值', max_length=20)
    ticket_id = models.CharField('申请单号', max_length=100)
    is_share = models.BooleanField('是否共享', default=False)
    workflow = models.ForeignKey(Workflow, related_name='workflow_for_ticket', verbose_name='流程关联', blank=True,
                                 on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='user_for_ticket', verbose_name='创建人', blank=True,
                             on_delete=models.CASCADE)

    createtime = models.DateTimeField('申请时间', auto_now_add=True)
    updatetime = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Tickets'
        verbose_name_plural = '流程单'


class WorkflowLog(models.Model):
    status = models.BooleanField('操作状态', default=True)
    description = models.CharField('流程备注', max_length=30)
    updatetime = models.DateTimeField('操作时间', auto_now_add=True)
    ticket = models.ForeignKey(Tickets, related_name='ticket_for_workflowLog', verbose_name='单据关联', blank=True,
                               on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='user_for_workflowLog', verbose_name='审批人员', blank=True,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'WorkflowLog'
        verbose_name_plural = '审批记录'


class ShareKey(models.Model):
    key = models.CharField('分享key', max_length=60)
    description = models.TextField('描述')
    is_use = models.BooleanField('是否有效', default=True)
    ticket = models.ForeignKey(Tickets, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    create_time = models.DateTimeField('创建时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.key