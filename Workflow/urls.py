# -*- coding:utf-8 -*-
# @Time: 2020/12/31
# @Author: 残源
# @Mail: canyuan@semf.top
from django.urls import path
from Workflow.views import workflowviews, logviews ,shareviews

urlpatterns = [
    path('ticket/list/<str:workflow_key>/', workflowviews.main_list),
    path('ticket/type/<str:ticket_key>/', workflowviews.ticket_list),
    path('ticket/action/<str:action>/<str:ticket_id>/', workflowviews.ticket_action),
    path('ticket/manyaction/<str:action>/', workflowviews.ticket_many_deny),
    path('ticket/details/<str:ticket_id>/', workflowviews.ticket_details),

    path('log/list/<str:ticket_id>/', logviews.ticket_checklog),

    path('share/create/<str:ticket_id>/', shareviews.ticket2share),
    path('share/deny/<str:ticket_id>/', shareviews.deny2share),
    path('share/view/<str:key>/', shareviews.share_details),

]