# -*- coding: utf-8 -*-
# @Time    : 2020/11/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : assetviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from Asset import models, serializers


@api_view(['GET'])
def select_asset_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = models.Asset.objects.filter(key__icontains=key)[10]
    list_count = list_get.count()
    serializers_get = serializers.AssetSelectSelectSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)