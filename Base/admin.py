# coding:utf-8

from django.contrib import admin
from . import models
# Register your models here.

admin.site.register(models.Department)
admin.site.register(models.Person)
admin.site.register(models.Client)
admin.site.register(models.Domain)
admin.site.register(models.Vlan)