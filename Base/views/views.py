# coding:utf-8
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.middleware import csrf
from captcha.conf import settings
from captcha.helpers import captcha_image_url, captcha_audio_url
from captcha.models import CaptchaStore
from django.http import HttpResponse
import json
# Create your views here.


def index(request):
    return HttpResponseRedirect('/static/index.html')


def admin(request):
    return HttpResponseRedirect('/static/admin/index.html')


def get_token(request):
    json_data = {
        "code": 0
        , "msg": ""
        , "data": {}
    }
    token = csrf.get_token(request)
    json_data['data']['token'] = token
    return JsonResponse(json_data)

