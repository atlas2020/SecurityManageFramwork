# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py


from django.urls import path
from .views import views, department, person, client, domain, vlan, permission, area, u_passwd


urlpatterns = [
    path('', views.index, name='index'),
    # path('admin', views.admin),
    path('get_token/', views.get_token, name='get_token'),

    path('base/list/department/', department.department_list, name='department_list'),
    path('base/create/department/', department.department_create, name='department_create'),
    path('base/update/department/<str:department_id>/', department.department_update, name='department_update'),
    path('base/delete/department/<str:department_id>/', department.department_delete, name='department_delete'),

    path('base/list/person/', person.person_list, name='person_list'),
    path('base/select/person/', person.person_select, name='person_select'),
    path('base/create/person/', person.person_create, name='person_create'),
    path('base/update/person/<str:person_id>/', person.person_update, name='person_update'),
    path('base/delete/person/<str:person_id>/', person.person_delete, name='person_delete'),

    path('base/list/permissionlist/', permission.permission_list, name='permission_list'),
    path('base/create/permissioncreate/', permission.permission_create , name='permission_create'),
    path('base/update/permission/<str:permission_id>/', permission.permission_update, name='permission_update'),
    path('base/delete/permission/<str:permission_id>/', permission.permission_delete, name='permission_delete'),

    path('base/list/arealist/', area.area_list, name='area_list'),
    path('base/create/areacreate/', area.area_create , name='area_create'),
    path('base/update/area/<str:area_id>/', area.area_update, name='area_update'),
    path('base/delete/area/<str:area_id>/', area.area_delete, name='area_delete'),

    path('base/list/client/', client.client_list, name='client_list'),
    path('base/create/client/', client.client_create, name='client_create'),
    path('base/update/client/<str:client_id>/', client.client_update, name='client_update'),
    path('base/delete/client/<str:client_id>/', client.client_delete, name='client_delete'),

    path('base/list/domainlist/', domain.domain_list , name='domain_list'),
    path('base/create/domaincreate/', domain.domain_create , name='domain_create'),
    path('base/update/domain/<str:domain_id>/', domain.domain_update, name='domain_update'),
    path('base/delete/domain/<str:domain_id>/', domain.domain_delete, name='domain_delete'),

    path('base/list/vlanlist/', vlan.vlan_list, name='vlan_list'),
    path('base/create/vlancreate/', vlan.vlan_create, name='vlan_create'),
    path('base/update/vlan/<str:vlan_id>/', vlan.vlan_update, name='vlan_update'),
    path('base/delete/vlan/<str:vlan_id>/', vlan.vlan_delete, name='vlan_delete'),

    path('base/admin/login/', u_passwd.create_views),
    path('base/username/list/', u_passwd.username_list),
    path('base/upasswd/list/<str:username_id>/', u_passwd.u_passwd_list),
]